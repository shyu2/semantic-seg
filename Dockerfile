FROM pytorch/pytorch
RUN apt-get update
RUN apt-get install -y libsm6 libxext6 libxrender-dev libgl1-mesa-glx vim git curl wget
ADD https://gitlab.com/shyu2/arclab/-/raw/main/environment.yml /tmp/environment.yml
RUN conda env update -f /tmp/environment.yml
RUN conda init bash
RUN echo "source activate" >> ~/.bashrc
ENV PATH /opt/conda/bin:$PATH

